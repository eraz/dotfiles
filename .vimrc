" =================
" vim-plug settings
" =================

" Automatically install vim-plug.
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/vim-plug')
    " Vim help for vim-plug itself.
    Plug 'junegunn/vim-plug'

    " Defaults everyone can agree on.
    Plug 'tpope/vim-sensible'

    " Lean and mean status/tabline for vim.
    Plug 'vim-airline/vim-airline'

    " A collection of themes for vim-airline.
    Plug 'vim-airline/vim-airline-themes'

    " A Git wrapper so awesome, it should be illegal.
    Plug 'tpope/vim-fugitive'

    " fzf <3 vim
    Plug 'junegunn/fzf.vim'

    " A Vim alignment plugin.
    Plug 'junegunn/vim-easy-align'
call plug#end()

" Set airline theme.
let g:airline_theme = 'dark'

" Show powerline font symbols.
let g:airline_powerline_fonts = 1

" Powerline fint symbols are partially messed up.
" if !exists('g:airline_symbols')
"   let g:airline_symbols = {}
" endif
" let g:airline_symbols.space = "\ua0"


" Supercharge vim with fzf and ripgrep. https://medium.com/@crashybang/supercharge-vim-with-fzf-and-ripgrep-d4661fc853d2
let $FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'
nnoremap faf :Files<CR>
command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)
nnoremap faw :Find<CR>
nnoremap fae :Lines<CR>


" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)



"""
""" Vim Settings
"""

" Set nocompatable just because.
set nocompatible

" Enable UTF-8 characters.
scriptencoding utf-8
set encoding=utf-8

" Automatically remove trailing whitespace on save.
au BufWritePre * :%s/\s\+$//e

" Prevent continue prompts.
set cmdheight=2

" Create a new tab when swiching buffer.
set switchbuf=newtab

" Make backspace delete properly.
set backspace=2

" Disable noises.
set noerrorbells
set novisualbell

" Enable folding.
" setlocal foldmethod=syntax

" Disable backups and swap files.
set nobackup
set nowb
set noswapfile


"""
""" UI Settings
"""

" Show filename.
set title

" Show active command.
set showcmd

" Automatically read file if changed externally.
set autoread

" Enable syntax highlighting.
syntax on
colorscheme ron
set background=dark

" Show invisibles.
exec "set listchars=tab:\uB7\uB7,trail:\uB7,nbsp:~,eol:\uAC"
set list

" Set tab behavior.
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set noautoindent
set nocindent
set nosmartindent

" Highlight current line.
set cursorline

" Configure search interface.
set nohlsearch
set incsearch

" Show line numbers.
set number
set relativenumber
set ruler

" Disable wrapping.
set nowrap

" Improve macro performance by preventing redraws.
" set lazyredraw

" Replace vertical split bar.
set fillchars=vert:│
hi VertSplit cterm=NONE ctermfg=NONE ctermbg=NONE



"""
""" Keybindings and Shortcuts
"""

" Disable inconvenient keybindings.
map <S-k> <Nop>

" Set leader to spacebar.
nnoremap <SPACE> <Nop>
let mapleader="\\"
map <SPACE> <Leader>

" Leader Shortcuts
map <Leader>w :w<Enter>
map <Leader>q :q<Enter>

map <Leader>s :vs<Enter><C-w><Right>
map <Leader>vs :vs<Enter><C-w><Right>
map <Leader>hs :sp<Enter><C-w><Down>

map <Leader>r :so ~/.vimrc<Enter>

map <Leader>dt vat<Esc>`<df>`>F<df>
map <Leader>td :!tidy -mi -wrap 0 %

" Automatically use !make instead of make.
cnoreabbrev <expr> m ((getcmdtype() is# ':' && getcmdline() is# 'm') ? ('!make -j8') : ('m'))
cnoreabbrev <expr> M ((getcmdtype() is# ':' && getcmdline() is# 'M') ? ('!make -j8') : ('M'))
cnoreabbrev <expr> make ((getcmdtype() is# ':' && getcmdline() is# 'make') ? ('!make') : ('make'))
cnoreabbrev <expr> Make ((getcmdtype() is# ':' && getcmdline() is# 'Make') ? ('!make') : ('Make'))

" Automatically use !git instead of git.
cnoreabbrev <expr> git ((getcmdtype() is# ':' && getcmdline() is# 'git') ? ('!git') : ('git'))
cnoreabbrev <expr> Git ((getcmdtype() is# ':' && getcmdline() is# 'Git') ? ('!git') : ('Git'))

" Automatically use !just instead of just.
cnoreabbrev <expr> j ((getcmdtype() is# ':' && getcmdline() is# 'j') ? ('!just') : ('j'))
cnoreabbrev <expr> J ((getcmdtype() is# ':' && getcmdline() is# 'J') ? ('!just') : ('J'))
cnoreabbrev <expr> just ((getcmdtype() is# ':' && getcmdline() is# 'just') ? ('!just') : ('just'))
cnoreabbrev <expr> Just ((getcmdtype() is# ':' && getcmdline() is# 'Just') ? ('!just') : ('Just'))

# Enable/Disable settings.
setopt extendedglob nomatch
unsetopt appendhistory autocd beep notify

# History settings.
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Enable Vi key bindings.
bindkey -v

# Something added by compinstall.
zstyle :compinstall filename '/home/eraz/.zshrc'

# Enable basic autocompletion.
autoload -Uz compinit
compinit

# Import shell aliases.
source $HOME/.aliases

# Enable prompt themes.
# autoload -Uz promptinit
# promptinit
# prompt adam2

# Enable Git prompt.
source ~/.git-prompt.sh
setopt PROMPT_SUBST ; PS1='[%n@%m %c$(__git_ps1 " (%s)")]\$ '

# Enable arrow key autocompletion.
autoload -Uz history-substring-search
bindkey '\e[A' history-beginning-search-backward
bindkey '\e[B' history-beginning-search-forward

# Enable ibus functionality.
# export GTK_IM_MODULE=ibus
# export XMODIFIERS=@im=ibus
# export QT_IM_MODULE=ibus

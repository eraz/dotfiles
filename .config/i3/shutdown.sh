#!/bin/bash

# Exit if error occurs.
set -e

WIN_IDs=$(wmctrl -l | awk '$3 != "N/A" {print $1}')
for i in $WIN_IDs; do wmctrl -ic "$i"; done

# A window is still open! Stop the script.
if [[ $(wmctrl -l) ]]; then
    exit 1
fi

# Parse arguments.
OPTIONS=ore
PARSED=$(getopt --options=$OPTIONS --name "$0" -- "$@")
eval set -- "$PARSED"

# Run proper command.
while true; do
    case "$1" in
        -o) i3-msg exit; exit 0;;
        -r) systemctl reboot; exit 0;;
        -e) systemctl poweroff; exit 0;;
        --) shift; break;;
        *)  ;;
    esac
done

#!/bin/bash

# Exit if error occurs.
set -e

# Slight delay to update screen.
sleep 0.1
scrot /tmp/screen.png
convert /tmp/screen.png -scale 5% -scale 2000% /tmp/screen.png
[[ -f $1 ]] && convert /tmp/screen.png $1 -gravity center -composite -matte /tmp/screen.png
i3lock -e -f -i /tmp/screen.png && sleep 1
rm /tmp/screen.png

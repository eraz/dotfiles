#!/bin/bash

acpi -b | awk -F'[,:%]' '{print $2, $3}' | {
    read -r status capacity

    if [ "$status" = Discharging -a "$capacity" -lt 10 ]; then
        /bin/i3-nagbar -m "Battery at $capacity percent! Connect a charger before the computer is forced to hibernate."
    fi
}

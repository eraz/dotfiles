# Disable greeting message.
function fish_greeting; end
function fish_right_prompt; end;

if test -e ~/.aliases
    . ~/.aliases
end

# Add paths to $PATH.
set PATH ~/.local/bin $PATH
set PATH ~/.gem/ruby/2.5.0/bin $PATH

# oh-my-fish settings.

# bobthefish
set -g theme_display_ruby no
set -g theme_display_vi yes
set -g theme_show_exit_status yes
set -g theme_prompt_pwd_dir_length 0
set -g theme_project_dir_length 0
set -g theme_newline_cursor yes
